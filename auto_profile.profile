<?php
/**
 * @file
 * Provides primary Drupal hook implementations.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Implements hook_form_FORM_ID_alter(): install_configure_form().
 */
function auto_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Merge the defaults with those from profile .info file.
  $profile = auto_profile_info_parse();

  // Password fields cannot be set with #default_value so use jQuery.
  if (isset($profile['admin_account']['account']['pass'])) {
    drupal_add_js("jQuery(document).ready(function () {
      jQuery('input[name=\"account[pass][pass1]\"]').val('" . $profile['admin_account']['account']['pass'] . "');
      jQuery('input[name=\"account[pass][pass2]\"]').val('" . $profile['admin_account']['account']['pass'] . "');
    });", 'inline');

    unset($profile['admin_account']['account']['pass']);
  }

  auto_profile_recurse($profile, $form);
}

/**
 * Recursively replace the form #default_values with overrides.
 *
 * @param array An associative array of overrides.
 * @param element The current form element.
 */
function auto_profile_recurse($array, &$element) {
  foreach ($array as $key => $value) {
    // Check if they override key is found in the current element.
    if (isset($element[$key])) {
      // Set the #default_value if $value scalar or "update_status_module".
      if (!is_array($value) || $key == 'update_status_module') {
        $element[$key]['#default_value'] = $value;
      }
      else {
        auto_profile_recurse($array[$key], $element[$key]);
      }
    }
  }
}

/**
 * Parse "site" module info file and merge profile[] with defaults.
 *
 * @return
 *   An associative array of merged defaults.
 */
function auto_profile_info_parse() {
  $defaults = array(
    'site_information' => array(
      'site_name' => $_SERVER['SERVER_NAME'],
      'site_mail' => 'noreply@example.com',
    ),
    'admin_account' => array(
      'account' => array(
        'name' => 'a',
        'pass' => 'b',
        'mail' => 'noreply@example.com',
      ),
    ),
    'update_notifications' => array(
      'update_status_module' => array(
        1 => FALSE,
        2 => FALSE,
      ),
    ),
  );

  // Parse the "site" module .info file and check for a "profile" key.
  $file = drupal_get_path('module', 'site') . '/site.info';
  $info = drupal_parse_info_file($file);
  $profile = isset($info['profile']) ? $info['profile'] : array();

  // Override default site_name with name from site module.
  if (isset($info['name'])) {
    $defaults['site_information']['site_name'] = $info['name'];
  }

  // Replace defaults with overrides from .info file.
  return array_replace_recursive($defaults, $profile);
}
