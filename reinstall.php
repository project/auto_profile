<?php
/**
 * @file
 * Make reinstallation a breeze.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

define('DRUPAL_ROOT', getcwd());

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
try {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
  $tables = db_find_tables('%');
  foreach ($tables as $table) {
    db_drop_table($table);
  }
}
catch (Exception $e) {
}
header('Location: install.php?profile=auto_profile&locale=en');
